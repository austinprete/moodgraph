import random
from django.core.urlresolvers import reverse

from django.shortcuts import render, redirect


# Create your views here.
from landing.models import Subscriber


def landing_view(request):
    return render(request, "landing/index.html")


def success_view(request):
    return render(request, "landing/success.html")


def failure_view(request):
    return render(request, 'landing/failure.html')


def subscribe_handler(request):
    post = request.POST
    email = post['email']
    subscribers = Subscriber.objects.all()
    duplicate = False
    for subscriber in subscribers:
        if subscriber.email == email:
            duplicate = True
    if duplicate:
        return redirect(reverse('landing:failure'))
    subscriber = Subscriber()
    subscriber.email = post['email']
    subscriber.save()
    return redirect(reverse('landing:success'))