from django.conf.urls import patterns, include, url
from django.contrib import admin
from landing import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'untitled.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.landing_view, name='index'),
    url(r'^subscribe/$', views.subscribe_handler, name='subscribe'),
    url(r'^subscribe/success/$', views.success_view, name='success'),
    url(r'^subscribe/failure/$', views.failure_view, name='failure'),
)
