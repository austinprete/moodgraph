from django.shortcuts import render

# Create your views here.
def profile_view(request):
    return render(request, 'user/profile.html', {'logged_in': True, 'user': request.user})