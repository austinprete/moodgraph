from django.conf.urls import patterns, url

from user import views


urlpatterns = patterns('',
                       url(r'^$', views.profile_view, name="profile"),
)