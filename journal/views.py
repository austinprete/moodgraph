import datetime
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

# Create your views here.
from journal.models import JournalEntry


@login_required
def index_view(request):
    user = request.user
    entries = user.journalentry_set.all().order_by('-date')[:5]
    return render(request, 'journal/index.html', {'user': user, 'entries': entries,})



@login_required
def entry_submit_handler(request):
    post = request.POST
    entry = JournalEntry()
    entry.user = request.user
    mood = post['mood']
    entry.mood = mood
    if mood == 'Neutral':
        entry.rating = 0
    else:
        entry.rating = post['rating']
    if 'safe' in post:
        entry.safe = True
    else:
        entry.safe = False
    entry.description = post['description']
    entry.date = datetime.datetime.now()
    entry.save()
    return redirect(reverse('journal:index'))


@login_required
def entry_delete_handler(request):
    post = request.POST
    id = post['entry_id']
    user = request.user
    entry = user.journalentry_set.all().filter(id=int(id))
    entry.delete()
    return redirect(reverse('journal:index'))
