# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0003_auto_20150228_1710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journalentry',
            name='mood',
            field=models.CharField(choices=[('Angry', 'Angry'), ('Anxious', 'Anxious'), ('Disgusted', 'Disgusted'), ('Guilty', 'Guilty'), ('Happy', 'Happy'), ('Hated', 'Hated'), ('Hopeful', 'Hopeful'), ('Jealous', 'Jealous'), ('Neutral', 'Neutral'), ('Optimistic', 'Optimistic'), ('Peaceful', 'Peaceful'), ('Sad', 'Sad')], max_length=15),
            preserve_default=True,
        ),
    ]
