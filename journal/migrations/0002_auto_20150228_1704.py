# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('journal', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='journalentry',
            name='date',
            field=models.DateField(default=None),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='journalentry',
            name='mood',
            field=models.CharField(max_length=15, choices=[('Angry', 'Angry'), ('Anxious', 'Anxious'), ('Disgust', 'Disgusted'), ('Guilt', 'Guilty'), ('Happy', 'Happy'), ('Hate', 'Hated'), ('Hope', 'Hopeful'), ('Jealous', 'Jealous'), ('Neutral', 'Neutral'), ('Optimistic', 'Optimistic'), ('Peace', 'Peaceful'), ('Sad', 'Sad')]),
            preserve_default=True,
        ),
    ]
