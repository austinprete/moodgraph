# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='JournalEntry',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('mood', models.CharField(max_length=15, choices=[('Ang', 'Angry'), ('Anx', 'Anxious'), ('Di', 'Disgusted'), ('Gu', 'Guilty'), ('Ha', 'Happy'), ('Hat', 'Hated'), ('Ho', 'Hopeful'), ('Je', 'Jealous'), ('Ne', 'Neutral'), ('Op', 'Optimistic'), ('Pe', 'Peaceful'), ('Sad', 'Sad')])),
                ('rating', models.IntegerField()),
                ('safe', models.BooleanField()),
                ('description', models.TextField(max_length=200)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
