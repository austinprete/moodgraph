from django.contrib import admin

# Register your models here.
from journal.models import JournalEntry

class EntryAdmin(admin.ModelAdmin):
    pass

admin.site.register(JournalEntry, EntryAdmin)