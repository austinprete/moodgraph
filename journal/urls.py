from django.conf.urls import patterns, url

from journal import views


urlpatterns = patterns('',
                       url(r'^$', views.index_view, name='index'),
                       url(r'^submit/', views.entry_submit_handler, name='submit'),
                       url(r'^delete/', views.entry_delete_handler, name='delete'),
)