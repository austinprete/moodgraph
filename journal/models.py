from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class JournalEntry(models.Model):
    user = models.ForeignKey(User)
    mood_choices = [
        ('Angry', 'Angry'),
        ('Anxious', 'Anxious'),
        ('Disgusted', 'Disgusted'),
        ('Guilty', 'Guilty'),
        ('Happy', 'Happy'),
        ('Hated', 'Hated'),
        ('Hopeful', 'Hopeful'),
        ('Jealous', 'Jealous'),
        ('Neutral', 'Neutral'),
        ('Optimistic', 'Optimistic'),
        ('Peaceful', 'Peaceful'),
        ('Sad', 'Sad'),
    ]
    mood = models.CharField(max_length=15, choices=mood_choices)
    rating = models.IntegerField()
    safe = models.BooleanField()
    description = models.TextField(max_length=200)
    date = models.DateTimeField()

    def __str__(self):
        return self.user.username + " - " + self.mood + " (%d)" % self.rating