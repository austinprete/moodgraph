from django.contrib.auth.decorators import user_passes_test
from django.contrib.auth.models import User
from django.core import mail
from django.core.mail import send_mail
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

# Create your views here.
from landing.models import Subscriber


@user_passes_test(lambda u: u.is_superuser)
def index_view(request):
    return render(request, 'admin_area/index.html')


@user_passes_test(lambda u: u.is_superuser)
def users_view(request):
    users = User.objects.all()
    return render(request, 'admin_area/users.html', {'users': users,})


@user_passes_test(lambda u: u.is_superuser)
def subscribers_view(request):
    subscribers = Subscriber.objects.all()
    return render(request, 'admin_area/subscribers.html', {'subscribers': subscribers,})


@user_passes_test(lambda u: u.is_superuser)
def message_subscribers_handler(request):
    post = request.POST
    user = request.user
    subscribers = Subscriber.objects.all()
    if subscribers:
        connection = mail.get_connection()
        for subscriber in subscribers:
            send_mail(post['subject'], post['message'], 'admin@moodgraph.net', [subscriber.email,], connection=connection)
    return redirect(reverse('admin_area:index'))