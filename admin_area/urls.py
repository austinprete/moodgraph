from django.conf.urls import patterns, url
from admin_area import views

urlpatterns = patterns('',
                       url(r'^$', views.index_view, name="index"),
                       url(r'^users/$', views.users_view, name="users"),
                       url(r'^subscribers/$', views.subscribers_view, name="subscribers"),
                       url(r'^subscribers/message/', views.message_subscribers_handler, name='message_subscribers'),
)