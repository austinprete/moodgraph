from django.conf.urls import patterns, url

from home import views


urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'untitled.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^about/$', views.about_view, name='about'),
                       url(r'^home/$', views.homepage_view, name='home'),
                       url(r'^$', views.index_view, name='index'),
)
