from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect

# Create your views here.
@login_required
def homepage_view(request):
    user = request.user
    return render(request, 'home/home.html', {'user': user,})


def index_view(request):
    return render(request, 'home/index.html')


def about_view(request):
    return render(request, 'home/about.html')
