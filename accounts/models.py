from django.conf import settings
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
class PersonalUser(models.Model):
    user = models.OneToOneField(User)
    gender_choices = [
        ('MA', 'Male'),
        ('FE', 'Female'),
    ]
    gender = models.CharField(max_length=10, choices=gender_choices)
    birthdate = models.DateField(null=True)
    age = models.IntegerField(null=True)


class SupportUser(models.Model):
    user = models.OneToOneField(PersonalUser)
    code = models.IntegerField()
    phone_number = models.CharField(max_length=20)
