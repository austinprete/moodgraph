from time import strptime, mktime
import datetime

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from django.shortcuts import render, redirect


# Create your views here.
from accounts.models import PersonalUser


def login_view(request):
    return render(request, 'accounts/login.html')

def login_handler(request):
    post = request.POST
    username = post['username']
    password = post['password']
    user = authenticate(username=username, password=password)
    if user is not None:
        if user.is_active:
            login(request, user)
            if user.first_name != "" and hasattr(user, 'personaluser'):
                return redirect(reverse('home:home'))
            else:
                return redirect(reverse('accounts:new_user'))
        else:
            message = "This account is inactive."
    else:
        message = "Username or password does not match our records."
    return render(request, 'accounts/login.html', {'message': message,})


def create_account_handler(request):
    post = request.POST
    username = post['username']
    email = post['email']
    password = post['password']
    try:
        User.objects.create_user(username, email, password)
    except:
        message = "Username is already taken."
        return render(request, 'accounts/login.html', {'message': message,})
    user = authenticate(username=username, password=password)
    login(request, user)
    if 'support' in post:
        return redirect(reverse('accounts:new_support'))
    return redirect(reverse('accounts:new_user'))


@login_required
def logout_handler(request):
    logout(request)
    return redirect(reverse('home:index'))


@login_required
def profile_view(request):
    user = request.user
    if user.first_name != "":
        name = user.first_name
    else:
        name = user.username
    return render(request, 'accounts/profile.html', {'user': user, 'name': name, })


@login_required
def new_user_view(request):
    return render(request, 'accounts/newuser.html')


@login_required
def new_user_handler(request):
    post = request.POST
    user = request.user
    user.first_name = post['first_name']
    user.last_name = post['last_name']
    user.save()
    personal_user = PersonalUser(user=user)
    personal_user.save()
    personal_user.gender = post['gender']
    birthdate = strptime(post['birthdate'], "%m/%d/%Y")
    date = datetime.datetime.fromtimestamp(mktime(birthdate))
    personal_user.birthdate = date
    personal_user.age = (datetime.datetime.now() - date).days / 365
    personal_user.save()
    return redirect(reverse('accounts:profile'))


@login_required
def new_support_user_view(request):
    return render(request, 'accounts/newsupport.html')