# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20150228_0710'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personaluser',
            name='age',
            field=models.IntegerField(null=True),
            preserve_default=True,
        ),
    ]
