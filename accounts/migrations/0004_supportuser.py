# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0003_auto_20150228_0711'),
    ]

    operations = [
        migrations.CreateModel(
            name='SupportUser',
            fields=[
                ('id', models.AutoField(verbose_name='ID', primary_key=True, serialize=False, auto_created=True)),
                ('code', models.IntegerField()),
                ('phone_number', models.CharField(max_length=20)),
                ('user', models.OneToOneField(to='accounts.PersonalUser')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
