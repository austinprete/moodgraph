from django.conf.urls import patterns, url

from accounts import views


urlpatterns = patterns('',
                       url(r'^login/$', views.login_view, name='login'),
                       url(r'^login/submit/$', views.login_handler, name='submit'),
                       url(r'^logout/$', views.logout_handler, name='logout'),
                       url(r'^create/$', views.create_account_handler, name='create'),
                       url(r'^profile/$', views.profile_view, name='profile'),
                       url(r'^new/$', views.new_user_view, name='new_user'),
                       url(r'^new/submit/$', views.new_user_handler, name='submit_new_user'),
                       url(r'^new/support/', views.new_support_user_view, name='new_support'),
)