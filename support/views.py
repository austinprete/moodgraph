from django.shortcuts import render

# Create your views here.
def support_home_view(request):
    user = request.user
    return render(request, 'support/index.html', {'user': user,})