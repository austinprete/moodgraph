from django.conf.urls import patterns, include, url
from django.contrib import admin
from support import views

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'untitled.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', views.support_home_view, name='index'),
)
