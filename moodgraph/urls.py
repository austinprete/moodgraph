from django.conf.urls import patterns, include, url
from django.contrib import admin
import landing

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'untitled.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('landing.urls', namespace="landing")),
    url(r'^beta/', include('home.urls', namespace='home')),
    url(r'^beta/graphs/', include('graphs.urls', namespace='graphs')),
    url(r'^beta/support/', include('support.urls', namespace='support')),
    url(r'^beta/user/', include('user.urls', namespace='user')),
    url(r'^beta/accounts/', include('accounts.urls', namespace='accounts')),
    url(r'^beta/journal/', include('journal.urls', namespace='journal')),
    url(r'^beta/admin/', include('admin_area.urls', namespace='admin_area')),
)
