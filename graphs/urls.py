from django.conf.urls import url, patterns

from graphs import views

urlpatterns = patterns('',
                       # Examples:
                       # url(r'^$', 'untitled.views.home', name='home'),
                       # url(r'^blog/', include('blog.urls')),
                       url(r'^$', views.graphs_home_view, name='index'),
)
