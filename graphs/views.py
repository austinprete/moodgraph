from django.contrib.auth.decorators import login_required
from django.shortcuts import render

# Create your views here.
@login_required
def graphs_home_view(request):
    user = request.user
    entries = user.journalentry_set.all().order_by('-date')[:7]

    ratings = []
    dates = []
    for entry in reversed(entries):
        if moodIsPositive(entry.mood):
            ratings.append(entry.rating)
        else:
            ratings.append(0 - entry.rating)
        date = entry.date.strftime('%m/%d/%y')
        dates.append(date)

    entries2 = user.journalentry_set.all().order_by('-date')
    ratings2 = []
    dates2 = []
    date_placeholder = None
    rating = 0
    counter = 0
    days = 0
    for index in range(0, len(entries2)):
        if days <= 7:
            entry = entries2[index]
            if date_placeholder == None:
                if index == len(entries2) - 1:
                    dates2.append(entry.date.strftime("%m/%d/%Y"))
                    ratings2.append(get_rating(entry))
                    days += 1
                else:
                    date_placeholder = entry.date.strftime("%m/%d/%Y")
                    rating += get_rating(entry)
                    counter += 1
            elif date_placeholder == entry.date.strftime("%m/%d/%Y"):
                if index == len(entries2) - 1:
                    rating += get_rating(entry)
                    counter += 1
                    average_rating = rating / counter
                    dates2.append(date_placeholder)
                    ratings2.append("{0:.2f}".format(average_rating))
                    days += 1
                else:
                    rating += get_rating(entry)
                    counter += 1
            else:
                if index == len(entries2) - 1:
                    dates2.append(entry.date.strftime("%m/%d/%Y"))
                    ratings2.append(get_rating(entry))
                else:
                    average_rating = rating / counter
                    dates2.append(date_placeholder)
                    ratings2.append("{0:.2f}".format(average_rating))
                    rating = get_rating(entry)
                    counter = 1
                    days += 1
                    date_placeholder = entry.date.strftime("%m/%d/%Y")
    return render(request, 'graphs/index.html',
                  {'user': user, 'ratings': ratings, 'dates': dates, 'ratings2': reversed(ratings2), 'dates2': reversed(dates2), })


def moodIsPositive(mood):
    if (mood == "Happy" or mood == "Hopeful" or mood == "Optimistic" or mood == "Peaceful"):
        return True
    else:
        return False


def get_rating(entry):
    if moodIsPositive(entry.mood):
        return entry.rating
    else:
        return (0-entry.rating)